/*
 * This file contains all auxiliary functions/code that is used in both the main code and test running code.
 */

const fs = require('fs');

function readFile(pathName) {
  try {
    // sticking with the sync version, we're not reading gigabytes here
    return fs.readFileSync(pathName, 'utf8');
  } catch (error) {
    this.error(error, 'Error at "readFile"');
  }
}

const reverseString = (str) => str.split('').reverse().join('');

/*
 *  Console printing for visibility
 */
const colors = {
  DEFAULT: "\x1b[0m",
  RED: "\x1b[31m",
  GREEN: "\x1b[32m",
  BG_RED: "\x1b[41m",
  BG_GREEN: "\x1b[42m",
  YELLOW: "\x1b[33m"
};

function fail(testNumber, msg) {
  console.log(`   ${colors.BG_RED}   ${colors.DEFAULT} Test #${testNumber} ${colors.RED}FAILED! ${colors.DEFAULT}${msg}`);
}

function pass(testNumber) {
  console.log(`   ${colors.BG_GREEN}   ${colors.DEFAULT} Test #${testNumber} ${colors.GREEN}passed. ${colors.DEFAULT}`);
}

function notice(msg) {
  console.log(`${colors.YELLOW}${msg}${colors.DEFAULT}`);
}

function printGreen(message) {
  console.log(`${colors.GREEN}${message}${colors.DEFAULT}`);
}

function printRed(message) {
  console.log(`${colors.RED}${message}${colors.DEFAULT}`);
}



module.exports = {
  readFile,
  reverseString,
  colors,
  fail,
  pass,
  notice,
  printGreen,
  printRed
}
