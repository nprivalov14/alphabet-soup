const wordSearcher = require('./word-searcher.js');
const { readFile, fail, pass, notice, printGreen, printRed } = require('./utils.js');

const TEST_DIRECTORY = './test-files/';

notice('Tests starting...\n');

let passedTests = 0;

test(1);
test(2);
test(3);
test(4);
test(5);
test(6);

if (passedTests === 6) {
  printGreen('\nAll tests passed!\n');
} else {
  printRed(`\nOne or more tests failed.\n`);
}

/*
 * Auxiliary Functions
 */

function test(testNumber) {
  const expected = readFile(`${TEST_DIRECTORY}/${testNumber}output.txt`).split('\n');
  const results = wordSearcher.solve(`${TEST_DIRECTORY}/${testNumber}.txt`).split('\n');

  const printResults = () => {
    notice(' Expected:');
    console.log(expected.join('\n'));
    console.log();
    notice(' Result:');
    console.log(results.join('\n'));
    console.log();
  }

  let passed = true;

  if (expected.length !== results.length) {
    fail(testNumber, 'Expected output does not match generated result in length!');
    passed = false;
    printResults();
    return;
  }

  expected.forEach((row, index) => {
    if (row !== results[index]) {
      fail(testNumber, 'Generated result is incorrect!');
      passed = false;
      printResults();
      return;
    }
  });

  if (passed) {
    pass(testNumber);
    passedTests++;
  }
}

