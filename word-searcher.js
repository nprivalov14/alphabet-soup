/*
 *  This file contains all relevant code for the alphabet-soup/word search challenge.
 *
 *  The test "runtime" is contained within `try-soup.js`.
 *  It can be run using `node try-soup.js`.
 *  (written and ran with the latest Node LTS version, v14.17.0)
 * 
 *  Tests inputs/outputs are contained within the `test-files` folder.
 */

const { readFile, reverseString } = require('./utils.js');

function solve(pathName) {
  const { rows, cols, gridRows, gridCols, wordsToFind } = digest(pathName);

  // for both columns and rows, only search within the row when the word hasn't been found yet

  // search in rows
  gridRows.forEach((row, index) => {
    Object.keys(wordsToFind).forEach(word => {
      if (!wordsToFind[word].found && wordsToFind[word].searchRows) {
        findInRow(row, index, word, wordsToFind[word]);
      }
    });
  });
  // search in columns pivoted into rows
  gridCols.forEach((col, index) => {
    Object.keys(wordsToFind).forEach(word => {
      if (!wordsToFind[word].found && wordsToFind[word].searchCols) {
        findInRow(col, index, word, wordsToFind[word], true);
      }
    });
  });

  // search for any remaining wordsToFind diagonally
  if (Object.values(wordsToFind).some(wordObj => !wordObj.found)) {
    gridRows.forEach((row, rowIndex) => {

      Object.keys(wordsToFind).forEach(word => {
        // only remaining wordsToFind but also only if it fits in the grid (and if it was a reverse result that could be replaced by a normal result)
        if ((wordsToFind[word].reverseResult || !wordsToFind[word].found) && wordsToFind[word].searchCols && wordsToFind[word].searchRows) {
          const startLetter = word[0],
                endLetter = wordsToFind[word].reverse[0];

          if (row.includes(startLetter)) {
            findDiagonals(gridRows, rows, cols, word, rowIndex, wordsToFind[word]);
          } else if (!wordsToFind[word].found && row.includes(endLetter)) {
            findDiagonals(gridRows, rows, cols, wordsToFind[word].reverse, rowIndex, wordsToFind[word], true);
          }
        }
      });
    });
  }


  // build result strings
  const results = [];
  Object.keys(wordsToFind).forEach(word => {
    if (wordsToFind[word].found) {
      results.push(`${word} ${wordsToFind[word].start} ${wordsToFind[word].end}`);
    }
  });

  return results.join('\n');
}

module.exports = {
  solve
}



/**
 * Breaks down the file with the given name into a working set of data to search for wordsToFind.
 */
function digest(pathName) {
  const rawData = readFile(pathName).split('\n');
  const [rows, cols] = rawData.shift().split('x').map(n => Number.parseInt(n));
  const gridRows = rawData.slice(0, rows).map(s => s.replace(/\s/g, ''));
  const words = rawData.slice(rows);

  // be able to reapply horizontal row searching logic
  const gridCols = new Array(cols).fill('').map((col, colIndex) => {
    gridRows.forEach(row => col += row[colIndex]);
    return col;
  });
  
  const wordsToFind = {};
  // initially the naming was the other way around, but seeing `words[word]` repeatedly hurts the eyes
  words.forEach(word => {
    wordsToFind[word] = {
      start: null,
      end: null,
      found: false, // could do `!word.start && !word.end` but this is more readable.
      searchRows: word.length <= cols,
      searchCols: word.length <= rows,
      reverse: reverseString(word) // used for the diagonals exclusively, surprisingly
    }
  });

  return { rows, cols, gridRows, gridCols, wordsToFind };
}

/**
 * Locates any possible matches within a row. Can be used with columns pivoted as rows by setting `column` true when called.
 */
function findInRow(row, rowIndex, word, wordObj, column = false) {
  const reverse = wordObj.reverse,
        targetWord = row.includes(word) ? word : row.includes(reverse) ? reverse : null;
  
  if (!targetWord) return;

  const start = row.indexOf(targetWord);
  // check to see if the rest of the word exists in the row
  if (!row.endsWith(targetWord, start + targetWord.length)) {
    return;
  }
  const end = start + targetWord.length - 1;

  if (targetWord === reverse) { // is a reverse word find
    if (column) {
      wordObj.start = `${end}:${rowIndex}`;
      wordObj.end = `${start}:${rowIndex}`;
    } else {
      wordObj.start = `${rowIndex}:${end}`;
      wordObj.end = `${rowIndex}:${start}`; 
    }
  } else { // not reverse word find
    if (column) {
      wordObj.start = `${start}:${rowIndex}`;
      wordObj.end = `${end}:${rowIndex}`;
    } else {
      wordObj.start = `${rowIndex}:${start}`;
      wordObj.end = `${rowIndex}:${end}`; 
    }
  }
  wordObj.found = true;
}

/**
 * Searches through for any matches diagonally.
 * 
 * This is done by first calculating possible "end" coordinates from the detected first letter of the word.
 * If the last letter of the word matches the letter at the "end" coordinates, it is worth checking the letters in between.
 * The letters in between are then checked to see if they spell the target word.
 */
function findDiagonals(gridRows, rows, cols, word, startRow, wordObject, reverse = false) {
  const lenIncl = word.length - 1; // "length inclusive" -- regular length isn't used nearly as often as this would be.

  if (wordObject.found && !wordObject.reverseResult) return;

  const checkDiagonal = (startCol, endRow, endCol, rowModifier, colModifier) => {
    if (gridRows[endRow][endCol] !== word[lenIncl]) return;

    let stringIndex = 0,
        currRow = startRow,
        currCol = startCol;

    // digging through the grid to check for a match
    while (stringIndex > -1 && stringIndex < lenIncl) {
      if (gridRows[currRow][currCol] !== word[stringIndex]) {
        stringIndex = -1;
      } else {
        stringIndex++;
        currRow += rowModifier;
        currCol += colModifier;
      }
    }

    // all letters matched up
    if (stringIndex === lenIncl) {
      wordObject.start = `${startRow}:${startCol}`;
      wordObject.end = `${endRow}:${endCol}`;
      wordObject.found = true;
      if (reverse) {
        wordObject.reverseResult = true;
      } else {
        wordObject.reverseResult = false;
      }
    }
  }

  for (let startCol = 0; startCol < cols; startCol++) {
    // up and left
    if ((startRow - lenIncl >= 0) && (startCol - lenIncl >= 0)) {
      checkDiagonal(startCol, (startRow - lenIncl), (startCol - lenIncl), -1, -1);
    }

    // up and right
    if ((startRow - lenIncl >= 0) && (startCol + lenIncl < cols)) {
      checkDiagonal(startCol, (startRow - lenIncl), (startCol + lenIncl), -1, 1);
    }

    // down and left
    if ((startRow + lenIncl < rows) && (startCol - lenIncl >= 0)) {
      checkDiagonal(startCol, (startRow + lenIncl), (startCol - lenIncl), 1, -1);
    }

    // down and right
    if ((startRow + lenIncl < rows) && (startCol + lenIncl < cols)) {
      checkDiagonal(startCol, (startRow + lenIncl), (startCol + lenIncl), 1, 1);
    }
  }
}
